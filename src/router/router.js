import { createWebHistory, createRouter } from "vue-router";
import Home from "../components/login.vue";
import tableInfo from "@/components/table_info.vue";

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/table",
        name: "table",
        component: tableInfo,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
